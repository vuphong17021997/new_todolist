import React from "react";
import { View, Text, Button } from "react-native";
class HomeScreen extends React.Component {
    static navigationOptions = {
        title: '',
    };
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' ,backgroundColor:'#22281F' }}>
                <Text style={{fontSize:30,color:'#D8E5D1'}}>Chuyển trang</Text>
                <Button style={{fontSize:20}}
                    title="To Do List"
                    onPress={() => {
                        this.props.navigation.navigate('List', {
                            titleParam: 'To Do List',
                        });
                    }}
                />
            </View>
        );
    }
}

export default HomeScreen
