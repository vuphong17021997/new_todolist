import { domain } from "../constants"
export default function deleteDataApi(data) {
    return new Promise((resolve, reject) => {
      const url = domain + '/tasks/'+ data
      fetch(url, {
        method: "DELETE"
      })
        .then((response) =>response.json())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  
